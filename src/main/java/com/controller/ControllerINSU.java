package com.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.Service.ServiceIn;
import com.model.Model;

@Controller
public class ControllerINSU { 
	
	
   /* @Autowired  
     Service  servic; 
    @RequestMapping(value="/save",method = RequestMethod.GET) 
    public ModelAndView showform()
    {
    servic.getEmployees(null); 
    return new ModelAndView("index");  
    }
    */
	@Autowired  
    ServiceIn  servic;
    @RequestMapping(value="/save",method = RequestMethod.POST)  
    public String save(@ModelAttribute("model") Model model,Map mod)
    {  
    	mod.put("model", model);
    	servic.save(model); 
        return "success";  
    }  
}
    	
    	
        
      
    