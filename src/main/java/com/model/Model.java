package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="healthcare")
public class Model {
	@Column(name="fname")
	private String FullName;
	@Column(name="age")
	private int Age;
	@Column(name="gender")
	private String Gendar;
	@Column(name="htension")
	private String Hypertension;
	@Column(name="bpressure")
	private String Bloodpressure;
	@Column(name="bsugar")
	private String Bloodsugar;
	@Column(name="oweight")
	private String Overweight;
	@Column(name="smoking")
	private String Smoking;
	@Column(name="alcohal")
	private String Alcohol;
	@Column(name="dex")
	private String Dailyexercise;
	@Column(name="drugs")
	private String Drugs;

	public String getFullName() {
		return FullName;
	}

	public void setFullName(String fullName) {
		FullName = fullName;
	}

	public int getAge() {
		return Age;
	}

	public void setAge(int age) {
		Age = age;
	}

	public String getGendar() {
		return Gendar;
	}

	public void setGendar(String gendar) {
		Gendar = gendar;
	}

	public String getHypertension() {
		return Hypertension;
	}

	public void setHypertension(String hypertension) {
		Hypertension = hypertension;
	}

	public String getBloodpressure() {
		return Bloodpressure;
	}

	public void setBloodpressure(String bloodpressure) {
		Bloodpressure = bloodpressure;
	}

	public String getBloodsugar() {
		return Bloodsugar;
	}

	public void setBloodsugar(String bloodsugar) {
		Bloodsugar = bloodsugar;
	}

	public String getOverweight() {
		return Overweight;
	}

	public void setOverweight(String overweight) {
		Overweight = overweight;
	}

	public String getSmoking() {
		return Smoking;
	}

	public void setSmoking(String smoking) {
		Smoking = smoking;
	}

	public String getAlcohol() {
		return Alcohol;
	}

	public void setAlcohol(String alcohol) {
		Alcohol = alcohol;
	}

	public String getDailyexercise() {
		return Dailyexercise;
	}

	public void setDailyexercise(String dailyexercise) {
		Dailyexercise = dailyexercise;
	}

	public String getDrugs() {
		return Drugs;
	}

	public void setDrugs(String drugs) {
		Drugs = drugs;
	}

}
