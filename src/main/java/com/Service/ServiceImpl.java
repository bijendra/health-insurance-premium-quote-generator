package com.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.Dao;
import com.model.Model;

@Service
public class ServiceImpl implements ServiceIn {

	@Autowired
	Dao dao;

	@Override
	public void save(Model m) {
		dao.save(m);
	}
}
