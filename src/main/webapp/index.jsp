<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<body>
	<h2>Norman is looking for a health insurance quote using this
		application</h2>

	<form:form method="post" action="save" commandName="comm">
		Full Name:<form:input path="name"/><br />
		<br /> Gender:<form:radiobutton path="male"/>Male 
		<form:radiobutton path="female"/>Female<br />
		<br /> Age :<form:input path="age"/><br />
		<br /> Current health:</br>
		<br /> Hypertension: <form:select path="hypertension">
			<form:option value="Yes"></form:option>
			<form:option value="No"></form:option>
		</form:select><br />
		<br /> Bloodpressure: <form:select path="bloodpressure">
			<form:option value="Yes"></form:option>
			<form:option value="No"></form:option>
		</form:select><br />
		<br /> Bloodsugar: <form:select path="bloodsugar">
			<form:option value="Yes"></form:option>
			<form:option value="No"></form:option>
		</form:select><br />
		<br /> Overweight: <form:select path="overweight">
			<form:option value="Yes"></form:option>
			<form:option value="No"></form:option>
		</form:select><br />
		<br /> Smoking: <form:select path="smoking">
			<form:option value="Yes"></form:option>
			<form:option value="No"></form:option>
		</form:select><br />
		<br /> Habits:</br>
		<br /> Alcohal: <form:select path="alcohal">
			<form:option value="Yes"></form:option>
			<form:option value="No"></form:option>
		</form:select><br />
		<br /> DailyExcercise: <form:select path="dailyex">
			<form:option value="Yes"></form:option>
			<form:option value="No"></form:option>
		</form:select><br />
		<br /> Drugs: <form:select path="drugs">
			<form:option value="Yes"></form:option>
			<form:option value="No"></form:option>
		</form:select><br />
		<br /> <br />
		<br /> <input type="submit" value="Submit" />
	</form:form>
</body>
</html>
